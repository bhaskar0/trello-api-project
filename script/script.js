let apiKey = "a8b3a46e482a943fa2cf6b653054491e";
let token = "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8";
let boardId = "5e8c2856041baa1165c28d8d";
let listId = "5e8c2926e4057a5f7edbf357";

function getCardsAndAddToDOM() {
  let allCards = fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`,
    { method: "GET" }
  )
    .then((response) => response.json())
    .then((res) => {
      res.forEach((card) => {
        generateCard(card.name, card.id);
      });
    })
    .catch((error) => console.log(error));
}

document.addEventListener("DOMContentLoaded", function () {
  getCardsAndAddToDOM();
});

let count = 0;
let button = document.getElementById("addButton");
button.addEventListener("click", formGenerate);

function formGenerate(e) {
  const newDiv = document.createElement("div");
  newDiv.classList.add("bg-white", "ml-0","rounded","shadow-md", "mb-2");
  newDiv.id = "cardAddDiv";

  const counter = document.createElement("div");
  counter.className = "bg-yellow-500 m-4";

  const addCardBody = document.createElement("div");
  addCardBody.classList.add("m-2", "bg-white");

  const form = document.createElement("form");
  form.classList.add("form", "p-2", "bg-white");

  const cardBody = document.createElement("input");
  cardBody.classList.add("card-body", "border","w-full","border-solid","border-black","rounded","h-10");
  cardBody.setAttribute("placeholder", "Enter the card");
  cardBody.setAttribute("type", "text");
  cardBody.setAttribute("id", "card-name");
  cardBody.setAttribute("name", "card-name");
  cardBody.required = true;

  const submitButton = document.createElement("button");
  submitButton.classList.add("Submit", "bg-green-500","hover:bg-green-700","text-white","font-bold","rounded", "w-1/4", "mt-4","mr-10","h-10");
  submitButton.innerHTML = "Add Card";
  submitButton.type = "submit";

  const closeIcon = document.createElement("button");
  closeIcon.className = "closeClass float-right mt-4 bg-white hover:text-red-500 cursor-pointer";
  closeIcon.innerHTML = "X";
  closeIcon.addEventListener("click", function removeform() {
    newDiv.remove();
    button.style.display = "inline";
  });

  form.append(cardBody, submitButton, closeIcon);
  addCardBody.append(form);
  newDiv.append(addCardBody);


  document.querySelector(".list").insertBefore(newDiv, e.target);
  e.target.style.display = "none";

  form.addEventListener(
    "submit",
    function (event) {
      event.preventDefault();
      if (form.checkValidity() === false) {
        event.stopPropagation();
      } else {
        let cardName = event.target.elements[0].value;
        console.log(cardName);
        event.target.parentElement.parentElement.remove();
        createNewCard(cardName);
        button.style.display = "inline";
      }
    },
    false
  );
}

function createNewCard(cardName) {
  fetch(
    `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}
        &key=${apiKey}&token=${token}`,
    { method: "POST" }
  )
    .then((card) => card.json())
    .then((card) => {
      generateCard(card.name, card.id);
    })
    .catch((error) => console.log(error));
}

// //generating card

function generateCard(cardName, cardId) {
  const newCard = document.createElement("div");
  newCard.classList.add(
    "bg-white",
    "shadow-md",
    "rounded",
    "ml-0",
    "mr-0",
    "m-4",
    "mb-2",
    "card",
    "flex"
  );
  newCard.id = cardId;
  newCard.setAttribute("data-name", cardName);

  const newCardTitle = document.createElement("li");
  newCardTitle.classList.add(
    "list-none",
    "m-4",
    "w-auto",
    "cursor-pointer",
    "flex-grow"
  );
  newCardTitle.appendChild(document.createTextNode(cardName));
  newCardTitle.id = "card" + count++;

  const deleteBtn = document.createElement("button");
  deleteBtn.className = "bg-white hover:text-red-500 rounded w-10 float-right delete";
  deleteBtn.appendChild(document.createTextNode("X"));
  deleteBtn.addEventListener("click", removeCard);

  newCard.append(newCardTitle, deleteBtn);
  document.getElementById("cards-list").append(newCard);
  const card = document.getElementById(newCardTitle.id);
  card.addEventListener("click", openCard);
  counter();
}

function counter() {
  setTimeout(function () {
    const counter = document.createElement("div");
    counter.className = "absolute right-0 top-0 bg-white p-2 m-1 mr-2 rounded w-20";
    const counterCards = document.getElementById("cards-list")
      .childElementCount;
    // console.log(counterCards);
    counter.innerHTML = "Cards" + " : " + counterCards;

    document.getElementById("hello").append(counter);
  }, 0);
}
//removing card
function removeCard(e) {
  const deleteCardId = e.target.parentElement.id;
  console.log(deleteCardId);
  fetch(
    `https://api.trello.com/1/cards/${deleteCardId}?key=${apiKey}&token=${token}`,
    { method: "DELETE" }
  ).then((deletedCard) => {
    if (confirm("Are You Sure?")) e.target.parentElement.remove();
    counter();
  });
}
// Opening Cards
function openCard(event) {
  let checklistCount = 0;
  if (document.querySelector(".modal") !== null) {
    document.querySelector(".modal").remove();
  }
  //modal creation
  const cardId = event.target.parentElement.id;
  // console.log(cardId)

  const modalBackground = document.createElement("div");
  modalBackground.className =
    "absolute top-0 left-0 bg-black w-screen min-h-screen opacity-75 modal";

  const modalMain = document.createElement("div");
  modalMain.className =
    "m-auto mt-10 mb-10 bg-white w-2/5 rounded-md opacity-100 p-4 m-4 modalMain";
  modalMain.id = cardId;

  const modalTitleDiv = document.createElement("div");
  modalTitleDiv.className =
    "bg-white h-auto modalTitleDiv rounded flex justify-between";
  // modalTitleDiv.innerHTML="title-Div"

  const modalTitle = document.createElement("h2");
  modalTitle.innerHTML = event.target.parentElement.dataset.name;
  modalTitle.className = "text-3xl";

  const modalContent = document.createElement("div");
  modalContent.className = "bg-white min-h-full pt-4 h-auto";
  // modalContent.innerHTML="content"
  modalContent.id = "modalContent";

  const createChecklistbtn = document.createElement("button");
  createChecklistbtn.className =
    "bg-gray-300 hover:bg-gray-700 hover:text-white mt-4 mb-4 rounded shadow-md h-auto p-2 rounded";
  createChecklistbtn.id = "createCheckList";
  createChecklistbtn.innerHTML = "Create Checklist";

  const closeIcon = document.createElement("button");
  closeIcon.className = "closeClass bg-white hover:text-red-500 font-bold rounded-md cursor-pointer";
  closeIcon.innerHTML = "X";
  closeIcon.addEventListener("click", function removeModal() {
    modalBackground.style.display = "none";
  });

  modalTitleDiv.append(modalTitle, closeIcon);
  modalContent.append(createChecklistbtn);
  modalMain.append(modalTitleDiv, modalContent);
  modalBackground.append(modalMain);
  document.getElementById("hello").append(modalBackground);

  fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((res) => res.json())
    .then((res) => {
      res.forEach((checklist) =>
        generateChecklist(checklist.name, checklist.id, checklist.checkItems)
      );
    })
    .then(() => {
      createChecklistbtn.addEventListener("click", checklistForm);
    })
    .catch((error) => {
      console.log(error);
    });
}

function checklistForm(e) {
  // console.log(e)
  const newDiv = document.createElement("div");
  newDiv.classList.add("bg-none");
  newDiv.id = "checklistDiv";

  const addChecklistBody = document.createElement("div");
  addChecklistBody.classList.add("bg-white");

  const form = document.createElement("form");
  form.classList.add("form", "m-2", "p-2", "bg-white");

  const checkListBody = document.createElement("input");
  checkListBody.classList.add("card-body", "block","w-full","border","border-solid","border-black");
  checkListBody.setAttribute("placeholder", "Enter the Checklist");
  checkListBody.setAttribute("type", "text");
  checkListBody.setAttribute("id", "card-name");
  checkListBody.setAttribute("name", "card-name");
  checkListBody.required = true;

  const submitButton = document.createElement("button");
  submitButton.classList.add("Submit", "bg-green-500","hover:bg-green-700","text-white","shadow-md","rounded", "w-1/4","mt-4", "h-10");
  submitButton.innerHTML = "Add Checklist";
  submitButton.type = "submit";

  const closeIcon = document.createElement("button");
  closeIcon.className = "closeClass bg-white hover:text-red-500 float-right m-4 cursor-pointer w-8";
  closeIcon.innerHTML = "X";
  closeIcon.addEventListener("click", function removeform() {
    newDiv.remove();
    e.target.style.display = "inline";
  });

  form.append(checkListBody, submitButton, closeIcon);
  addChecklistBody.append(form);
  newDiv.append(addChecklistBody);

  // form.addEventListener("submit",createCard)

  document.getElementById(e.target.parentElement.id).append(newDiv, e.target);
  e.target.style.display = "none";

  form.addEventListener(
    "submit",
    function (event) {
      event.preventDefault();
      if (form.checkValidity() === false) {
        event.stopPropagation();
        form.classList.add("was-validated");
      } else {
        let checklistName = event.target.elements[0].value;
        console.log(checklistName);
        event.target.parentElement.parentElement.remove();
        createChecklist(checklistName);
        document.getElementById("createCheckList").style.display = "block";
      }
    },
    false
  );
}

function createChecklist(checklistName) {
  let cardId = document.querySelector(".modalMain").id;
  fetch(
    `https://api.trello.com/1/checklists?idCard=${cardId}&name=${checklistName}
    &key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((res) => res.json())
    .then((res) => generateChecklist(res.name, res.id, res.checkItems))
    .catch((error) => {
      console.log(error);
    });
}

//add checklists to the dom

function generateChecklist(checklistName, checklistId, checklistItems) {
  const checklist = document.createElement("div");
  checklist.className =
    "relative bg-white border w-full h-auto shadow-md mb-4 p-4";
  checklist.id = checklistId;

  const checklistTitle = document.createElement("h2");
  checklistTitle.className = "text-2xl m-0 mb-4";
  checklistTitle.innerHTML = checklistName;

  const checklistOptions = document.createElement("div");
  checklistOptions.className = "relative bg-white";

  const addChecklistItemsbtn = document.createElement("button");
  addChecklistItemsbtn.className =
    "relative left-0 p-2 bg-gray-300 hover:bg-gray-700 hover:text-white shadow-md h-10 rounded addChecklistItemBtn";
  addChecklistItemsbtn.innerHTML = "Add Items";
  addChecklistItemsbtn.addEventListener("click", addChecklistItemsForm);

  const deleteChecklistbtn = document.createElement("button");
  deleteChecklistbtn.className =
    "absolute right-0 top-0 bg-gray-300 hover:bg-gray-700 hover:text-white shadow-md p-2 m-4 rounded";
  deleteChecklistbtn.innerHTML = "Delete";
  deleteChecklistbtn.addEventListener("click", deleteChecklist);

  checklistOptions.append(addChecklistItemsbtn);
  checklist.append(checklistTitle, checklistOptions, deleteChecklistbtn);
  document.getElementById("modalContent").append(checklist);
  checklistItems.forEach((checkitem) => {
    generateCheckItems(checkitem);
  });
}

// //deleting the checklist

function deleteChecklist(event) {
  const deleteChecklistId = event.target.parentElement.id;

  fetch(
    `https://api.trello.com/1/checklists/${deleteChecklistId}?key=${apiKey}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      event.target.parentElement.remove();
    })
    .catch((err) => console.error(err));
}

//add checklist Item Form

function addChecklistItemsForm(e) {
  const newDiv = document.createElement("div");
  newDiv.classList.add("bg-white","mt-4");
  newDiv.id = "checklistItemsDiv";

  const form = document.createElement("form");
  form.classList.add("form", "bg-white");

  const checkListItemsBody = document.createElement("input");
  checkListItemsBody.classList.add(
    "checkListItems-body",
    "w-full",
    "block",
    "border",
    "border-solid",
    "border-black"
  );
  checkListItemsBody.setAttribute("placeholder", "Enter the checklist item");
  checkListItemsBody.required = true;

  const submitButton = document.createElement("button");
  submitButton.classList.add("Submit", "bg-green-500","hover:bg-green-700","text-white","rounded","p-2","font-bold", "m-4");
  submitButton.innerHTML = "Add Item";
  submitButton.type = "submit";

  const closeIcon = document.createElement("button");
  closeIcon.className = "closeClass bg-white hover:text-red-500 cursor-pointer font-bold";
  closeIcon.innerHTML = "X";
  closeIcon.addEventListener("click", function removeform() {
    newDiv.remove();
    e.target.style.display = "block";
  });

  form.append(checkListItemsBody, submitButton, closeIcon);
  newDiv.append(form);

  // e.target.style.display="none";
  e.target.parentElement.append(newDiv);
  form.addEventListener(
    "submit",
    function (event) {
      event.preventDefault();
      if (form.checkValidity() === false) {
        event.stopPropagation();
        form.classList.add("was-validated");
      } else {
        let checklistItemName = event.target.elements[0].value;
        let checklistId = this.parentElement.parentElement.parentElement.id;
        event.target.parentElement.remove();
        createChecklistItems(checklistItemName, checklistId);
      }
    },
    false
  );
}

// post checklist Items to DOM
function createChecklistItems(checklistItemName, checklistId) {
  fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checklistItemName}
    &key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((res) => res.json())
    .then((res) => generateCheckItems(res))
    .catch((error) => {
      console.log(error);
    });
}

function generateCheckItems(checkItem) {
  const checkItemDiv = document.createElement("div");
  checkItemDiv.className = "bg-white w-full my-4 flex items-center";
  checkItemDiv.id = checkItem.id;

  const checkboxInput = document.createElement("input");
  checkboxInput.setAttribute("type", "checkbox");
  checkboxInput.className = "inputBox bg-white cursor-pointer";
  checkboxInput.addEventListener("change", updateStatus);
  // checkboxInput.id=checkItem.id

  if (checkItem.state === "complete") {
    checkboxInput.checked = true;
  } else {
    checkboxInput.checked = false;
  }

  const checkboxInputLabel = document.createElement("label");
  checkboxInputLabel.className = "bg-white labelInput cursor-pointer mx-4";
  checkboxInputLabel.setAttribute("for", checkItem.id);
  checkboxInputLabel.innerHTML = checkItem.name;
  // checkboxInputLabel.addEventListener("click",updateStatus)

  const closeIcon = document.createElement("button");
  closeIcon.className = "closeClass bg-white hover:text-red-500 cursor-pointer";
  closeIcon.innerHTML = "X";
  closeIcon.addEventListener("click", deleteCheckItems);

  checkItemDiv.append(checkboxInput, checkboxInputLabel, closeIcon);
  document.getElementById(checkItem.idChecklist).append(checkItemDiv);
}

// delete CheckItems

function deleteCheckItems() {
  console.log("item deleted");
  const checklistId = event.target.parentElement.parentElement.id;
  const checkitemId = event.target.parentElement.id;

  event.target.parentElement.remove();

  fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${apiKey}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      this.parentElement.remove();
    })
    .catch((err) => console.error(err));
}

//update the status of checkItems
function updateStatus(e) {
  const checkItemId = e.target.parentElement.id;
  const checklistId = e.target.parentElement.parentElement.id;
  const cardId =
    e.target.parentElement.parentElement.parentElement.parentElement.id;
  const checkitemState = this.checked === true ? "complete" : "incomplete";
  console.log(checkitemState);
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${token}&state=${checkitemState}`,
    {
      method: "PUT",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => console.log(text))
    .catch((err) => console.error(err));
}
